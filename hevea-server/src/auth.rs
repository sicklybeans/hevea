use diesel::prelude::*;
use hyper::header::HeaderValue;
use hyper::HeaderMap;
use hyper::header::AUTHORIZATION;
use hyper::{Response, Body};
use jsonwebtoken::{decode};
use serde::{Deserialize, Serialize};
use std::{
  sync::Arc,
  time::{Duration, SystemTime, Instant},
};

use crate::{
  // api::requests::LoginRequest,
  common::get_utc_timestamp_secs,
  config::Config,
  errors::LookupError,
  requests::parse_login_request,
  response::empty_response,
  models::User,
  schema::user,
};

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
  exp: u64,
}

// pub async fn login(cfg: Arc<Config>, req: Request<Body>) -> Response<Body> {
//   match parse_login_request(req).await {
//     Some((username, password)) => {
//       match get_user(cfg.clone(), &req.username) {

//       }
//       match verify_password(&req.password, &usr.password)? {
//         true => generate_token(cfg),
//         false => Err(Error::newf_with_code("Invalid password", 400)),
//       }
//     },
//     None => error_response(400, "Invalid request")
//   }

// }
// pub fn handle_login(cfg: Arc<Config>, req: LoginRequest) -> Result<String, Error> {
//   let usr = get_user(cfg.clone(), &req.username)?;
//   match verify_password(&req.password, &usr.password)? {
//     true => generate_token(cfg),
//     false => Err(Error::newf_with_code("Invalid password", 400)),
//   }
// }

fn get_user(cfg: Arc<Config>, username: &str) -> Result<User, LookupError> {
  let conn = cfg.make_conn()?;
  user::table
    .filter(user::username.eq(username))
    .load::<User>(&conn)?
    .pop()
    .ok_or(LookupError::NotFoundError)
}

// fn verify_password(password: &str, hashed: &str) -> Result<bool, Error> {
//   println!("hash: {:?}", bcrypt::hash(password, 10));
//   bcrypt::verify(password, hashed).map_err(|e| {
//     eprintln!("Bcrypt error!: {:?}", e);
//     Error::newf_with_code("Unknown server error", 500)
//   })
// }

// fn generate_token(cfg: Arc<Config>) -> Result<String, Error> {
//   let claims = Claims {
//     exp: cfg.generate_exp_time_secs(),
//   };
//   encode(&cfg.jwt_header, &claims, &cfg.jwt_encoding_key).map_err(|e| {
//     eprintln!("Token creation failed: {:?}", e);
//     Error::newf_with_code("Internal server error", 500)
//   })
// }

pub fn check_authentication(cfg: Arc<Config>, headers: &HeaderMap) -> Response<Body> {
  match extract_token_from_header(headers) {
    Ok(token) => match get_token_exp_time(cfg, &token) {
      Some(exp_time) => {
        if exp_time > get_utc_timestamp_secs() {
          empty_response(200)
        } else {
          empty_response(401)
        }
      },
      None => empty_response(400)
    },
    Err(code) => empty_response(code)
  }
}

fn extract_token_from_header(headers: &HeaderMap<HeaderValue>) -> Result<String, u16> {
  match headers.get(AUTHORIZATION) {
    None => Err(401),
    Some(value) => match value.to_str() {
      Ok(s) => {
        let parts: Vec<&str> = s.split(" ").collect();
        if parts.len() != 2 {
          Err(400)
        } else if parts[0] != "JWT" {
          Err(400)
        } else {
          Ok(parts[1].to_string())
        }
      }
      Err(_) => Err(400),
    },
  }
}

fn get_token_exp_time(cfg: Arc<Config>, token: &str) -> Option<u64> {
  match decode::<Claims>(token, &cfg.jwt_decoding_key, &cfg.jwt_validation) {
    Ok(decoded) => Some(decoded.claims.exp),
    Err(_) => None
  }
}
