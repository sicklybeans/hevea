use hyper::{Body, Request};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct LoginRequest {
  username: String,
  password: String,
}

pub async fn parse_login_request(req: Request<Body>) -> Option<(String, String)> {
  match hyper::body::to_bytes(req.into_body()).await {
    Ok(bytes) => match serde_json::from_slice::<LoginRequest>(&bytes) {
      Ok(result) => Some((result.username, result.password)),
      Err(_) => None
    },
    Err(_) => None
  }
}
