#[macro_use]
extern crate diesel;
use colored::Colorize;
use hyper::{
  service::{make_service_fn, service_fn},
  Body, HeaderMap, Method, Request, Response, Server, Uri
};
use std::{convert::Infallible, net::SocketAddr, sync::Arc, time::Instant};

mod auth;
mod common;
mod config;
mod errors;
mod models;
mod requests;
mod response;
mod schema;

use config::Config;
use response::empty_response;


/// Main routing function which routes HTTP requests to proper methods.
async fn main_router(cfg: Arc<Config>, uri: Uri, method: Method, headers: HeaderMap, body: Body) -> Response<Body> {
  let paths: Vec<&str> = uri.path().split('/').collect();
  if paths.len() < 2 {
    return empty_response(404)
  }

  match paths[1] {
    "auth" => auth::check_authentication(cfg, &headers),
    "api" => empty_response(501),
    "login" => empty_response(501),
    _ => empty_response(404),
  }
}

/// Wrapper for `main_router` used in development to log results to stdout.
async fn router_wrapper(cfg: Arc<Config>, req: Request<Body>) -> Result<Response<Body>, Infallible> {
  let (parts, body) = req.into_parts();
  let log_message = format!("{} {}", parts.method, parts.uri);

  let t0 = Instant::now();
  let response = main_router(cfg, parts.uri, parts.method, parts.headers, body).await;
  let elapsed = t0.elapsed().as_millis();

  // Log results using error-based colors
  let code = response.status().as_u16();
  let color = if code < 300 {
    "green"
  } else if code < 400 {
    "yellow"
  } else {
    "red"
  };
  println!("{}: {} ({} ms)", code.to_string().color(color), log_message, elapsed);
  Ok(response)
}


#[tokio::main]
async fn main() {
  let cfg = Arc::new(Config::new());

  let addr = SocketAddr::from(([127, 0, 0, 1], cfg.port));

  let make_svc = make_service_fn(move |_| {
    let cfg = cfg.clone();

    async move { Ok::<_, Infallible>(service_fn(move |req| router_wrapper(cfg.clone(), req))) }
  });

  let server = Server::bind(&addr).serve(make_svc);

  // Run this server for... forever!
  if let Err(e) = server.await {
    eprintln!("server error: {}", e);
  }
}
