pub enum LookupError {
  ConnectionError,
  NotFoundError
}

impl From<diesel::ConnectionError> for LookupError {
  fn from(err: diesel::ConnectionError) -> LookupError {
    eprintln!("Diesel connection error: {:?}", err);
    LookupError::ConnectionError
  }
}

impl From<diesel::result::Error> for LookupError {
  fn from(err: diesel::result::Error) -> LookupError {
    eprintln!("Diesel result error: {:?}", err);
    LookupError::ConnectionError
  }
}
