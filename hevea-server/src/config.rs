use diesel::prelude::*;
use dotenv::dotenv;
use jsonwebtoken::{DecodingKey, EncodingKey, Header, Validation};
use std::{
  env,
  time::{Duration, SystemTime},
};

pub type DBConn = diesel::mysql::MysqlConnection;
pub type DBType = diesel::mysql::Mysql;

pub struct Config {
  pub port: u16,
  pub jwt_encoding_key: EncodingKey,
  pub jwt_decoding_key: DecodingKey<'static>,
  pub jwt_header: Header,
  pub jwt_validation: Validation,
  jwt_expire_time: Duration,
  database_uri: String,
  database_schema: String,
}

impl Config {
  pub fn new() -> Config {
    dotenv().ok(); // this is a slow method.
    let port = env::var("HEVEA_PORT")
      .expect("HEVEA_PORT must be set")
      .parse::<u16>()
      .expect("Invalid port value");
    let jwt_secret = env::var("SECRET_KEY").expect("SECRET_KEY must be set");
    let db_schema = env::var("DATABASE_SCHEMA").expect("DATABASE_SCHEMA must be set");
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let db_uri = format!("{}/{}", db_url, db_schema);
    Config {
      port,
      jwt_encoding_key: EncodingKey::from_secret(&jwt_secret.as_ref()),
      jwt_decoding_key: DecodingKey::from_secret(&jwt_secret.as_ref()).into_static(), //.expect("Invalid JWT secret").into_static(),
      jwt_header: Header::default(),
      jwt_validation: Validation::default(),
      jwt_expire_time: Duration::from_secs(60 * 60 * 24),
      database_uri: db_uri,
      database_schema: db_schema,
    }
  }

  /// Generates expiration timestamp for a new JWT in number of seconds since unix epoch.
  pub fn generate_exp_time_secs(&self) -> u64 {
    (SystemTime::now() + self.jwt_expire_time)
      .duration_since(SystemTime::UNIX_EPOCH)
      .unwrap()
      .as_secs() as u64
  }

  pub fn make_conn(&self) -> Result<DBConn, diesel::result::ConnectionError> {
    DBConn::establish(&self.database_uri)
  }
}
