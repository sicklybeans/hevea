use std::time::SystemTime;

pub fn get_utc_timestamp_secs() -> u64 {
  SystemTime::now()
    .duration_since(SystemTime::UNIX_EPOCH)
    .unwrap()
    .as_secs() as u64
}
