#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ -e "/run/nginx.pid" ]; then
  echo "Reloading config for running server"
  sudo nginx -p "$SCRIPTPATH" -c nginx.conf -s reload
else
  echo "Starting up a new server"
  sudo nginx -p "$SCRIPTPATH" -c nginx.conf
fi
